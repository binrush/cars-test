import os
import tempfile
import time

import pytest
from peewee import *

from model import MODELS, CarMake, CarModel, CarSubModel, Car
from endpoints import app


SAMPLE_TIMESTAMP = 1600000000


@pytest.fixture
def client():
    app.config["TESTING"] = True

    with app.test_client() as client:
        yield client


@pytest.fixture(autouse=True)
def test_db():
    mock_db = SqliteDatabase(":memory:", pragmas={"foreign_keys": 1})

    mock_db.bind(MODELS, bind_refs=False, bind_backrefs=False)
    mock_db.connect()
    mock_db.create_tables(MODELS)
    yield
    mock_db.drop_tables(MODELS)
    mock_db.close()


@pytest.fixture
def sample_makes():
    CarMake.create(
        id="vaz",
        name="VAZ",
        active=True,
        created_at=SAMPLE_TIMESTAMP + 3600 * 24,
        updated_at=SAMPLE_TIMESTAMP + 3600 * 25,
    )
    CarMake.create(
        id="zaz",
        name="ZAZ",
        active=False,
        created_at=SAMPLE_TIMESTAMP,
        updated_at=SAMPLE_TIMESTAMP + 3600,
    )


@pytest.fixture
def sample_models(sample_makes):
    for id, name, make_id, active, created_at, updated_at in [
        ("2101", "2101", "vaz", True, 24, 25),
        ("2106", "2106", "vaz", False, 3, 5),
        ("965A", "964A", "zaz", True, 3, 6),
    ]:
        CarModel.create(
            id=id,
            name=name,
            make_id=make_id,
            active=active,
            created_at=SAMPLE_TIMESTAMP + 3600 * created_at,
            updated_at=SAMPLE_TIMESTAMP + 3600 * updated_at,
        )


@pytest.fixture
def sample_submodels(sample_models):
    for id, name, model_id, active, created_at, updated_at in [
        ("2111", "2111", "2101", True, 24, 25),
        ("2112", "2112", "2101", False, 24, 26),
        ("2106-1", "2106-1", "2106", True, 24, 30),
        ("965A-1", "965A-1", "965A", True, 24, 30),
    ]:
        CarSubModel.create(
            id=id,
            name=name,
            model_id=model_id,
            active=active,
            created_at=SAMPLE_TIMESTAMP + 3600 * created_at,
            updated_at=SAMPLE_TIMESTAMP + 3600 * updated_at,
        )


@pytest.fixture
def sample_cars(sample_submodels):
    samples = [
        {
            "id": "596bdfcc04c842619673fc180f38cc1c",
            "active": True,
            "year": 2000,
            "mileage": 100200,
            "price": 1050,
            "submodel_id": 2111,
            "body_type": "Van",
            "transmission": "Manual",
            "fuel_type": "Gas",
            "exterior_color": "Red",
            "created_at": SAMPLE_TIMESTAMP + 3600,
            "updated_at": SAMPLE_TIMESTAMP + 3600 * 2,
        },
        {
            "id": "8e3b015b7d2f4d1ba7f6fced9a3d270c",
            "active": True,
            "year": 2015,
            "mileage": 100234,
            "price": 100500,
            "submodel_id": 2111,
            "body_type": "Van",
            "transmission": "Manual",
            "fuel_type": "Gas",
            "exterior_color": "Green",
            "created_at": SAMPLE_TIMESTAMP + 3600,
            "updated_at": SAMPLE_TIMESTAMP + 3600 * 20,
        }
    ]
    for s in samples:
        Car.create(**s)


@pytest.mark.parametrize(
    "endpoint, expected",
    [
        [
            "/submodels",
            {
                "submodels": [
                    {
                        "id": "2111",
                        "name": "2111",
                        "created_at": SAMPLE_TIMESTAMP + 3600 * 24,
                        "updated_at": SAMPLE_TIMESTAMP + 3600 * 25,
                    }
                ]
            },
        ],
        [
            "/models",
            {
                "models": [
                    {
                        "id": "2101",
                        "name": "2101",
                        "created_at": SAMPLE_TIMESTAMP + 3600 * 24,
                        "updated_at": SAMPLE_TIMESTAMP + 3600 * 25,
                    }
                ]
            },
        ],
        [
            "/makes",
            {
                "makes": [
                    {
                        "id": "vaz",
                        "name": "VAZ",
                        "created_at": SAMPLE_TIMESTAMP + 3600 * 24,
                        "updated_at": SAMPLE_TIMESTAMP + 3600 * 25,
                    }
                ]
            },
        ],
    ],
)
def test_endpoints(client, sample_submodels, endpoint, expected):
    rv = client.get(endpoint)
    assert rv.get_json() == expected


def test_add(client, sample_submodels):
    rv = client.post(
        "/cars",
        data=dict(
            mileage=1000,
            year=2000,
            price=100,
            submodel_id="2111",
            body_type="Van",
            transmission="Manual",
            fuel_type="Gas",
            exterior_color="Black",
        ),
    )
    result = rv.get_json()
    assert result["success"]
    new_id = result["id"]
    new_car = Car.get(id=new_id)
    assert new_car.mileage == 1000
    assert new_car.year == 2000
    assert new_car.price == 100
    assert new_car.submodel_id == "2111"
    assert new_car.body_type == "Van"
    assert new_car.transmission == "Manual"
    assert new_car.fuel_type == "Gas"
    assert new_car.exterior_color == "Black"


@pytest.mark.parametrize(
    "request_data,expected_error",
    [
        [
            {
                "mileage": 1000,
                "price": 100,
                "submodel_id": "2111",
                "body_type": "Van",
                "transmission": "Manual",
                "fuel_type": "Gas",
                "exterior_color": "Black",
            },
            [{"loc": ["year"], "msg": "field required", "type": "value_error.missing"}],
        ],
        [
            {
                "mileage": 1000,
                "year": "2000",
                "price": "100",
                "submodel_id": "zzz",
                "body_type": "Van",
                "transmission": "Manual",
                "fuel_type": "Gas",
                "exterior_color": "Black",
            },
            "Unknown submodel",
        ],
    ],
)
def test_add_bad_request(client, sample_submodels, request_data, expected_error):
    rv = client.post("/cars", data=request_data)
    assert rv.status_code == 400
    result = rv.get_json()
    assert not result["success"]
    assert result["errors"] == expected_error


@pytest.mark.parametrize(
    "query, expected",
    [
        [
            "/cars",
            {
                "cars": [
                    {
                        "id": "8e3b015b7d2f4d1ba7f6fced9a3d270c",
                        "year": 2015,
                        "mileage": 100234,
                        "price": 100500,
                        "submodel": "2111",
                        "model": "2101",
                        "make": "VAZ",
                        "body_type": "Van",
                        "transmission": "Manual",
                        "fuel_type": "Gas",
                        "exterior_color": "Green",
                        "created_at": SAMPLE_TIMESTAMP + 3600,
                        "updated_at": SAMPLE_TIMESTAMP + 3600 * 20,
                    },
                    {
                        "id": "596bdfcc04c842619673fc180f38cc1c",
                        "year": 2000,
                        "mileage": 100200,
                        "price": 1050,
                        "submodel": "2111",
                        "model": "2101",
                        "make": "VAZ",
                        "body_type": "Van",
                        "transmission": "Manual",
                        "fuel_type": "Gas",
                        "exterior_color": "Red",
                        "created_at": SAMPLE_TIMESTAMP + 3600,
                        "updated_at": SAMPLE_TIMESTAMP + 3600 * 2,
                    }
                ]
            },
        ],
        [
            "/cars?price_to=2000",
            {
                "cars": [
                    {
                        "id": "596bdfcc04c842619673fc180f38cc1c",
                        "year": 2000,
                        "mileage": 100200,
                        "price": 1050,
                        "submodel": "2111",
                        "model": "2101",
                        "make": "VAZ",
                        "body_type": "Van",
                        "transmission": "Manual",
                        "fuel_type": "Gas",
                        "exterior_color": "Red",
                        "created_at": SAMPLE_TIMESTAMP + 3600,
                        "updated_at": SAMPLE_TIMESTAMP + 3600 * 2,
                    }
                ]
            },
        ]
    ],
)
def test_cars_list(client, sample_cars, query, expected):
    rv = client.get(query)
    assert rv.get_json() == expected
