import os
from datetime import datetime
import csv

from peewee import *


def adapt_bool(value):
    if value == "t":
        return True
    elif value == "f":
        return False
    raise ValueError("Wrong value for boolean")


def adapt_datetime(value):
    # FIXME also parse fractions of seconds
    # and time zone
    return datetime.strptime(value[:19], "%Y-%m-%d %H:%M:%S").timestamp()


db = SqliteDatabase(None, pragmas={"foreign_keys": 1})


class CarMake(Model):
    class Meta:
        database = db
        table_name = "makes"

    id = CharField(primary_key=True)
    name = CharField(null=False)
    active = BooleanField(null=False)
    created_at = IntegerField(null=False)
    updated_at = IntegerField(null=False)

    @classmethod
    def fetch_active(cls):
        return list(
            cls.select(cls.id, cls.name, cls.created_at, cls.updated_at)
            .where(cls.active)
            .dicts()
        )

    @classmethod
    def adapt_row(cls, row):
        return {
            "id": row["id"],
            "name": row["name"],
            "active": adapt_bool(row["active"]),
            "created_at": adapt_datetime(row["created_at"]),
            "updated_at": adapt_datetime(row["updated_at"]),
        }


class CarModel(Model):
    class Meta:
        database = db
        table_name = "models"

    id = CharField(primary_key=True)
    name = CharField(null=False)
    active = BooleanField(null=False)
    make = ForeignKeyField(CarMake, backref="models", lazy_load=False, null=False)
    created_at = IntegerField(null=False)
    updated_at = IntegerField(null=False)

    @classmethod
    def fetch_active(cls):
        return list(
            cls.select(cls.id, cls.name, cls.created_at, cls.updated_at)
            .join(CarMake)
            .where(CarMake.active, cls.active)
            .dicts()
        )

    @classmethod
    def adapt_row(cls, row):
        return {
            "id": row["id"],
            "name": row["name"],
            "active": adapt_bool(row["active"]),
            "make_id": row["make_id"],
            "created_at": adapt_datetime(row["created_at"]),
            "updated_at": adapt_datetime(row["updated_at"]),
        }


class CarSubModel(Model):
    class Meta:
        database = db
        table_name = "submodels"

    id = CharField(primary_key=True)
    name = CharField(null=False)
    active = BooleanField(null=False)
    model = ForeignKeyField(CarModel, backref="submodels", lazy_load=False, null=False)
    created_at = IntegerField(null=False)
    updated_at = IntegerField(null=False)

    @classmethod
    def fetch_active(cls):
        return list(
            cls.select(cls.id, cls.name, cls.created_at, cls.updated_at)
            .join(CarModel)
            .join(CarMake)
            .where(CarMake.active, CarModel.active, cls.active)
            .dicts()
        )

    @classmethod
    def adapt_row(cls, row):
        return {
            "id": row["id"],
            "name": row["name"],
            "active": adapt_bool(row["active"]),
            "model_id": row["model_id"],
            "created_at": adapt_datetime(row["created_at"]),
            "updated_at": adapt_datetime(row["updated_at"]),
        }


class Car(Model):
    class Meta:
        database = db
        table_name = "cars"

    id = CharField(primary_key=True)
    active = BooleanField(null=False)
    year = IntegerField(null=False)
    mileage = IntegerField(null=False)
    price = IntegerField(null=False)
    submodel = ForeignKeyField(
        CarSubModel, backref="submodel", lazy_load=False, null=False
    )
    body_type = CharField(null=False)
    transmission = CharField(null=False)
    fuel_type = CharField(null=False)
    exterior_color = CharField(null=False)
    created_at = IntegerField(null=False)
    updated_at = IntegerField(null=False)

    @classmethod
    def adapt_row(cls, row):
        return {
            "id": row["id"],
            "active": adapt_bool(row["active"]),
            "year": int(row["year"]),
            "mileage": int(row["mileage"]),
            "price": int(row["price"]),
            "submodel_id": row["submodel_id"],
            "body_type": row["body_type"],
            "transmission": row["transmission"],
            "fuel_type": row["fuel_type"],
            "exterior_color": row["exterior_color"],
            "created_at": adapt_datetime(row["created_at"]),
            "updated_at": adapt_datetime(row["updated_at"]),
        }

    @classmethod
    def fetch_active(
        cls, price_from=None, price_to=None, mileage_from=None, mileage_to=None
    ):
        q = (
            cls.select(
                cls.id,
                cls.year,
                cls.mileage,
                cls.price,
                CarSubModel.name.alias("submodel"),
                CarModel.name.alias("model"),
                CarMake.name.alias("make"),
                cls.body_type,
                cls.transmission,
                cls.fuel_type,
                cls.exterior_color,
                cls.created_at,
                cls.updated_at,
            )
            .join(CarSubModel)
            .join(CarModel)
            .join(CarMake)
            .where(Car.active, CarSubModel.active, CarModel.active, CarMake.active)
        )
        if price_from is not None:
            q = q.where(cls.price >= price_from)
        if price_to is not None:
            q = q.where(cls.price <= price_to)
        if mileage_from is not None:
            q = q.where(cls.mileage >= mileage_from)
        if mileage_to is not None:
            q = q.where(cls.mileage <= mileage_to)
        q = q.order_by(cls.updated_at.desc())
        return list(q.dicts())


MODELS = [CarMake, CarModel, CarSubModel, Car]


def from_csv(cls, path):
    with open(path) as f:
        reader = csv.DictReader(f)
        for row in reader:
            try:
                cls.insert(**cls.adapt_row(row)).execute()
            except ValueError:
                # FIXME: log error message
                pass


def import_data(datadir):
    with db.atomic():
        for m in MODELS:
            from_csv(m, os.path.join(datadir, m._meta.table_name + ".csv"))
