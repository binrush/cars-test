from time import time
from typing import Optional
from uuid import uuid4

from flask import Flask, request, abort, jsonify
from peewee import IntegrityError
from pydantic import BaseModel, Extra, ValidationError

from model import CarMake, CarModel, CarSubModel, Car


class CarAddRequest(BaseModel):
    year: int
    mileage: int
    price: int
    submodel_id: str
    body_type: str
    transmission: str
    fuel_type: str
    exterior_color: str

    class Config:
        extra: "forbid"


class CarListRequest(BaseModel):
    price_from: Optional[int]
    price_to: Optional[int]
    mileage_from: Optional[int]
    mileage_to: Optional[int]

    class Config:
        extra: "forbid"


app = Flask(__name__)


@app.errorhandler(400)
def invalid_request(e):
    return jsonify(e), 400


@app.route("/makes")
def makes():
    return {"makes": CarMake.fetch_active()}


@app.route("/models")
def models():
    return {"models": CarModel.fetch_active()}


@app.route("/submodels")
def submodels():
    return {"submodels": CarSubModel.fetch_active()}


def list_cars():
    try:
        params = CarListRequest(**request.args).dict()
        return {"cars": Car.fetch_active(**params)}
    except ValidationError as e:
        return {"success": False, "errors": e.errors()}, 400


def add_car():
    try:
        params = CarAddRequest(**request.form).dict()
        now = int(time())
        new_car = Car.create(
            id=uuid4().hex, updated_at=now, created_at=now, active=True, **params
        )
    except ValidationError as e:
        return {"success": False, "errors": e.errors()}, 400
    except IntegrityError as e:
        return {"success": False, "errors": "Unknown submodel"}, 400
    else:
        return {"success": True, "id": new_car.id}


@app.route("/cars", methods=["GET", "POST"])
def cars():
    if request.method == "POST":
        return add_car()
    else:
        return list_cars()
