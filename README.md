Running:

```
pipenv install
pipenv shell
python3 run.py <path_to_data_dir>
```

where `data_dir` is a directory with `*.csv` files.

Application will automatically load data to database
diring startup.
Database is located in `<project_dir>/cars.db`, this file
is removed during startup, so changes are not preserved


Running tests

```
pipend install --dev
pipenv shell
py.test
```
