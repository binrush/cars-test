from contextlib import suppress
import os
import sys

from endpoints import app
from model import CarMake, db, import_data, MODELS


def run():

    db_path = os.path.join(os.path.dirname(__file__), "cars.db")
    with suppress(FileNotFoundError):
        os.unlink(db_path)

    db.init(db_path)
    db.connect()
    db.create_tables(MODELS)

    import_data(sys.argv[1])

    app.run()


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: run.py <datadir>")
        exit(1)

    run()
